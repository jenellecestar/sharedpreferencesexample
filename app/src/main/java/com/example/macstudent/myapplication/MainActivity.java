package com.example.macstudent.myapplication;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    // 1. Create a shared preferences variable
    SharedPreferences prefs;

    // 1a. give your sharedpreference "file" a unique name
    // Any name is okay!
    public static final String PREFERENCES_NAME = "ABCDPreferences";


    // ENHANCEMENT: Create a ui variable for the edit text
    EditText e;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 2. Setup shared preferences variable
        // 1st argument = name of your SP file (PREFERNCES_NAME)
        // 2nd argument = options for the file (append, read only?, delete?)
        // MODE_PRIVATE (0) -> ONLY THIS APP CAN USE THE SHAREDPREFERNCES FILE
        prefs = getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        //prefs = getSharedPreferences("ABCDPreferences", 0);

        // ENHANCEMENT:  setup the editext variable
        e = (EditText) findViewById(R.id.editText);

    }

    public void saveData(View view) {
        // 3. Save something to shared preferences
        // ==========================================

        // 3a. create an "editor" object - this allows you to modify shared preferences
        SharedPreferences.Editor prefEditor = prefs.edit();

        // 3b. put something into the shared preferences
        /// (key, value)

        // uncomment this to get the original code
        //prefEditor.putString("name","KADEEM");

        // ENHANCEMENT1 - save whatever was written in the text box
        String n = e.getText().toString();
        prefEditor.putString("name",n);


        // 3c. SAVE YOUR CHANGES
        prefEditor.apply();

        Log.d("JENELLE", "i saved something!!");

        // UI NONSENSE: clear the text box after you save
        e.setText("");

    }

    public void fetchData(View view) {
        Log.d("JENELLE", "fetch button pressed!");

        // 4. Fetch something from shared preferences

        // 4a. get something out of shared preferences
        // (key, DEFAULTVALUE)
        // if key doesn't exist -->
        String n = prefs.getString("name", "");

        //int p = prefs.getInt("name", -1);
        //boolean p = prefs.getBoolean("name", false);

        // 4b. print it to the console
        Log.d("JENELLE", "Got something from SP: " + n);


    }

}

