# Shared Preferences Example

Simple demonstration of how to save and get data from Shared Preferences

Shared Preferences is local storage! It should only be used to store simple things (like configuration variables)
If you need to save something more complex, use sqlite database or external storage

![Scheme](screenshots/screenshot.png)



